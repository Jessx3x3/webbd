
<?php $conexion=mysqli_connect('localhost', 'root', '', 'mydb');?>

<?php include("includes/header.php") ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="title_m">Matriculados</h2>
                </div>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Run</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Curso</th>
                    <th>Editar</th>
                    <th>Desvincular</th>
                </tr>
                </thead>
                <?php
                    $sql="SELECT * from alumno";
                    $result=mysqli_query($conexion, $sql);

                    while($mostrar=mysqli_fetch_array($result)){
                ?>
                <tr>
                    <td><?php echo $mostrar['run']?></td>
                    <td><?php echo $mostrar['nombre']?></td>
                    <td><?php echo $mostrar['apellido']?></td>
                    <td><?php echo $mostrar['Curso_id']?></td>
                    <td><a href="edit.php?id=<?php echo $mostrar['id']?>" class="btn btn-success">Editar
                        <i class="far fa-marker"></i>
                    </td>
                    <td><a href="delete.php?id=<?php echo $mostrar['id']?>" class="btn btn-danger">Eliminar
                        <i class="far fa-trash-alt"></i>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>

    
<?php include("includes/footer.php") ?>