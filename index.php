<?php include("db.php");?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cursos</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="CSS/style.css">
</head>
<body>
    <div id="content" role="main" class="container-md">
        <header class="header">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="./index.php">Colegio</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="./viewInfo.php">Matriculados<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="./addStudent.php">Alumnos</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="./addCurso.php">Cursos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Profesores</a>
                            </li>
                        </ul>
                    </div>
                </nav>
        </header>
        <div class="row">
            <div class="col-lg-12">
                <img src="res/banner_admision.jpg" alt="" width="1110">
            </div>
        </div><br>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h2 class="title_m">Matricúlate!</h2>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-lg-3">
                <div class="card mb-3">
                    <img src="res/1ero.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Primero Medio</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque at tenetur saepe reprehenderit voluptatem sit facilis laborum, aut fugit hic illum quibusdam id eligendi! Fuga, debitis! Magni iusto hic blanditiis.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        <button type="button" class="btn btn-primary" onclick="methodFirst();">Matricúlate</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card mb-3">
                    <img src="res/2do.jpeg" class="card-img-top" alt="..." height="170">
                    <div class="card-body">
                        <h5 class="card-title">Segundo Medio</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Est voluptatum fugiat hic qui repudiandae, quos quis recusandae nulla facilis pariatur aspernatur, cupiditate excepturi laborum dolorem sapiente quam.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        <button type="button" class="btn btn-primary" id="btn_segMedio" onclick="methodSecond();">Matricúlate</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card mb-3">
                    <img src="res/3ero.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Tercero Medio</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi commodi explicabo, distinctio est voluptates nam voluptatem voluptate mollitia quae atque voluptatum repudiandae hic a tempore sit facilis tempora aliquid eaque.</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        <button type="button" class="btn btn-primary" id="btn_terMedio" onclick="methodthird();">Matricúlate</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card mb-3">
                    <img src="res/4to.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Cuarto Medio</h5>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Hic eaque dicta quaerat eos asperiores harum architecto nostrum aut, quos ab cumque cupiditate alias voluptatem veniam suscipit laborum voluptate! Ea, dolorem!</p>
                        <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                        <button type="button" class="btn btn-primary" id="btn_cuaMedio" onclick="methodFourth();">Matricúlate</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("includes/footer.php") ?>    