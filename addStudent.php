
<?php $conexion=mysqli_connect('localhost', 'root', '', 'mydb');?>
<?php include("includes/header.php") ?>
        <div class="row">
            <div class="col-lg-12">
                <img src="res/banner_admision.jpg" alt="" width="1110">
            </div>
        </div><br>
        <div class="row">
            <?php if(isset($_SESSION['message'])){?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            <?php
            }?>
            <div class="col-lg-12">
                <form action="save.php" method="POST">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="run">Run</label>
                        <input type="text" class="form-control" id="run" placeholder="12345678-9" name="run">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="curso">Curso</label>
                            <select id="curso" class="form-control" name="curso">
                                <option selected>Curso</option>
                                <?php
                                    $query = $conexion -> query("SELECT * FROM curso");
                                    while($valores = mysqli_fetch_array($query)){
                                        echo '<option value="'.$valores["id"].'">'.$valores["nivel"].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                                Clickéa
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Matricular</button>
                </form>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<?php include("includes/footer.php") ?>