<?php

    include("db.php");

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $query = "SELECT * FROM alumno WHERE id = $id";
        $result = mysqli_query($conexion, $query);

        if(mysqli_num_rows($result) == 1){
            $row = mysqli_fetch_array($result);
            $run = $row['run'];
            $nombre = $row['nombre'];
            $apellido = $row['apellido'];
            $curso = $row['Curso_id'];
        }
    }
    if(isset($_POST['update'])){
        $run = $_GET['run'];
        $nombre = $_GET['nombre'];
        $apellido = $_GET['apellido'];
        $curso = $_GET['curso'];

        $query = "UPDATE alumno SET run = '$run', nombre = '$nombre', apellido = '$apellido', Curso_id = '$curso' WHERE id = $id"; 
        mysqli_query($conexion, $query);

        $_SESSION['message'] = 'Alumno actualizado correctamente';
        $_SESSION['message_type'] = 'warning';

        header("Location: viewInfo.php");
    }
?>

<?php include("includes/header.php"); ?>
    <div class="container p-4">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="card card-body">
                    <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
                        <div class="form-group">
                            <input type="text"  name="run" value="<?php echo $run;?>" class="form-control" placeholder="Actualiza Run">
                        </div>
                        <div class="form-group">
                            <input type="text" name="nombre" value="<?php echo $nombre;?>" class="form-control" placeholder="Actualiza Nombre">
                        </div>
                        <div class="form-group">
                            <input type="text" name="apellido" value="<?php echo $apellido;?>" class="form-control" placeholder="Actualiza Apellido">
                        </div>
                        <div class="form-group">
                            <select name="curso" id="curso" class="form-control">
                                <option selected><?php echo $curso;?></option>
                                <option value="1">Primero Medio</option>
                                <option value="2">Segundo Medio</option>
                                <option value="3">Tercero Medio</option>
                                <option value="4">Cuarto Medio</option>
                            </select>
                        </div>
                        <button name="update" class="btn btn-success">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include("includes/footer.php"); ?>