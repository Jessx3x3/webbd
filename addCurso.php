
<?php include("db.php");?>

<?php include("includes/header.php"); ?>
    <div class="row">
        <div class="col-lg-12">
            <img src="res/banner_admision.jpg" alt="" width="1110">
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-body">
                <form action="saveCurso.php" method="POST">
                    <div class="form-group">
                        <input type="text"  name="curso" value="curso" id="curso" class="form-control" placeholder="Nombre Curso">
                    </div>
                    <div class="form-group">
                        <input type="number" name="capacidad" value="capacidad" id="capacidad" class="form-control" placeholder="Capacidad Curso">
                    </div>
                    <button name="update" class="btn btn-success">Actualizar</button>
                </form>
            </div>
        </div>
        <div class="col-lg-6">
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th>Curso</th>
                </tr>
                </thead>
                <?php
                    $sql="SELECT * from curso";
                    $result=mysqli_query($conexion, $sql);

                    while($mostrar=mysqli_fetch_array($result)){
                ?>
                <tr>
                    <td><?php echo $mostrar['nivel']?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>

<?php include("includes/footer.php"); ?>